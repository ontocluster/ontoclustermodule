﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OntoClusterConnector;
using OntoEntities;
using OntoEntities.DataClasses;
using OntologyClasses.DataClasses;

namespace OntoClusterConnectorTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SaveBaseEntities()
        {
            var classes = new Classes();
            var logStates = new LogStates();
            var relationTypes = new RelationTypes();
            var dbUpdater =
                GlobalData.DbUpdater;
            var result = dbUpdater.SaveClusterEntities(classes.ClassItems, EntityRole.Class);
            result.Result.Id.Should().Be(logStates.Success.Id);
            result = dbUpdater.SaveClusterEntities(logStates.LogStateEntities, EntityRole.Object);
            result.Result.Id.Should().Be(logStates.Success.Id);
            result = dbUpdater.SaveClusterEntities(relationTypes.RelationTypeEntities, EntityRole.RelationType);
            result.Result.Id.Should().Be(logStates.Success.Id);

        }

        [TestMethod]
        public void MigrateClasses()
        {
            var logStates = new LogStates();
            var resultList = GlobalData.DbSelectorPrevious.get_Data_Classes();
            var result = GlobalData.DbUpdater.SaveClusterEntities(resultList.Select(resultItem => new ClusterEntity
            {
                Id = resultItem.GUID,
                Name = resultItem.Name,
                ParentIds = new List<string> {resultItem.GUID_Parent},
                EntityRole = EntityRole.Class,
                DataType = DataType.String
            }).ToList(), EntityRole.Class);
            result.Result.Id.Should().Be(logStates.Success.Id);
        }

        [TestMethod]
        public void MigrateObjects()
        {
            var logStates = new LogStates();
            var resultList = GlobalData.DbSelectorPrevious.get_Data_Objects();
            var result = GlobalData.DbUpdater.SaveClusterEntities(resultList.Select(resultItem => new ClusterEntity
            {
                Id = resultItem.GUID,
                Name = resultItem.Name,
                ParentIds = new List<string> { resultItem.GUID_Parent },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            }).ToList(), EntityRole.Class);
            result.Result.Id.Should().Be(logStates.Success.Id);
        }

        [TestMethod]
        public void MigrateRelationTypes()
        {
            var logStates = new LogStates();
            var resultList = GlobalData.DbSelectorPrevious.get_Data_RelationTypes();
            var result = GlobalData.DbUpdater.SaveClusterEntities(resultList.Select(resultItem => new ClusterEntity
            {
                Id = resultItem.GUID,
                Name = resultItem.Name,
                EntityRole = EntityRole.RelationType,
                DataType = DataType.String
            }).ToList(), EntityRole.RelationType);
            result.Result.Id.Should().Be(logStates.Success.Id);
        }

        [TestMethod]
        public void MigrateAttributeTypes()
        {
            var logStates = new LogStates();
            var dataTypesPrevious = new clsDataTypes();

            var resultList = GlobalData.DbSelectorPrevious.get_Data_AttributeType();
            var result = GlobalData.DbUpdater.SaveClusterEntities(resultList.Select(resultItem => new ClusterEntity
            {
                Id = resultItem.GUID,
                Name = resultItem.Name,
                DataType = resultItem.GUID_Parent == dataTypesPrevious.DType_Bool.GUID ? DataType.Bit : 
                    resultItem.GUID_Parent == dataTypesPrevious.DType_DateTime.GUID ? DataType.DateTime :
                    resultItem.GUID_Parent == dataTypesPrevious.DType_Int.GUID ? DataType.Long : 
                    resultItem.GUID_Parent == dataTypesPrevious.DType_Real.GUID ? DataType.Double : 
                    DataType.String,
                EntityRole = EntityRole.Class
                
            }).ToList(), EntityRole.Class);
            result.Result.Id.Should().Be(logStates.Success.Id);
        }

        [TestMethod]
        public void MigrateAttributes()
        {
            var logStates = new LogStates();
            var dataTypes = new clsDataTypes();
            
            var resultList = GlobalData.DbSelectorPrevious.get_Data_ObjectAtt(null);
            var boolAttrib = resultList.Where(reultItem => reultItem.ID_DataType == dataTypes.DType_Bool.GUID) .Select(resultItem => new ClusterEntity
            {
                Id = resultItem.ID_Attribute,
                Name = resultItem.Val_Bit.Value ? "True" : "False",
                BoolVal = resultItem.Val_Bit,
                DataType = DataType.Bit,
                EntityRole = OntoEntities.EntityRole.Object
            }).ToList();

            var dateTimeAttrib = resultList.Where(reultItem => reultItem.ID_DataType == dataTypes.DType_DateTime.GUID).Select(resultItem => new ClusterEntity
            {
                Id = resultItem.ID_Attribute,
                Name = resultItem.Val_Date.ToString(),
                DateTimeVal = resultItem.Val_Datetime,
                DataType = DataType.DateTime,
                EntityRole = OntoEntities.EntityRole.Object
            }).ToList();

            var doubleAttrib = resultList.Where(reultItem => reultItem.ID_DataType == dataTypes.DType_Real.GUID).Select(resultItem => new ClusterEntity
            {
                Id = resultItem.ID_Attribute,
                Name = resultItem.Val_Double.ToString(),
                DoubleVal = resultItem.Val_Double,
                DataType = DataType.Double,
                EntityRole = OntoEntities.EntityRole.Object
            }).ToList();

            var intAttrib = resultList.Where(reultItem => reultItem.ID_DataType == dataTypes.DType_Int.GUID).Select(resultItem => new ClusterEntity
            {
                Id = resultItem.ID_Attribute,
                Name = resultItem.Val_Int.ToString(),
                LongVal = resultItem.Val_Int,
                DataType = DataType.Long,
                EntityRole = OntoEntities.EntityRole.Object
            }).ToList();

            var strAttrib = resultList.Where(reultItem => reultItem.ID_DataType == dataTypes.DType_String.GUID).Select(resultItem => new ClusterEntity
            {
                Id = resultItem.ID_Attribute,
                Name = resultItem.Val_String,
                DataType = DataType.String,
                EntityRole = OntoEntities.EntityRole.Object
            }).ToList();

            var result = GlobalData.DbUpdater.SaveClusterEntities(boolAttrib, EntityRole.Object);
            result.Result.Id.Should().Be(logStates.Success.Id);

            result = GlobalData.DbUpdater.SaveClusterEntities(dateTimeAttrib, EntityRole.Object);
            result.Result.Id.Should().Be(logStates.Success.Id);

            result = GlobalData.DbUpdater.SaveClusterEntities(intAttrib, EntityRole.Object);
            result.Result.Id.Should().Be(logStates.Success.Id);

            result = GlobalData.DbUpdater.SaveClusterEntities(doubleAttrib, EntityRole.Object);
            result.Result.Id.Should().Be(logStates.Success.Id);

            result = GlobalData.DbUpdater.SaveClusterEntities(strAttrib, EntityRole.Object);
            result.Result.Id.Should().Be(logStates.Success.Id);
        }

        [TestMethod]
        public void MigrateClassAtt()
        {
            var dbSelectorOld = GlobalData.DbSelectorPrevious;
            var dbSelector = GlobalData.DbSelector;
            var dbUpdater = GlobalData.DbUpdater;
            var logStates = new LogStates();

            var relationTypes = new RelationTypes();

            var resultPrevious = dbSelectorOld.get_Data_ClassAtt(null, true);
            var clusterConnectors = resultPrevious.Select(classAtt => new ClusterConnector
            {
                IdLeft = classAtt.ID_Class,
                EntityRoleLeft = EntityRole.Class,
                IdRight = classAtt.ID_AttributeType,
                EntityRoleRight = EntityRole.Class,
                IdRelation = relationTypes.BelongingAttribute.Id,
                ConnectorType = ConnectorType.ClassAttribute,
                WeightPrimary = classAtt.Min,
                WeightSecondary = classAtt.Min,
                WeightSecondaryBack = 0
            }).ToList();

            var entityResult = dbUpdater.SaveClusterConnectors(clusterConnectors);
            entityResult.Result.Id.Should().Be(logStates.Success.Id);


        }

        [TestMethod]
        public void MigrateClassRel()
        {
            var dbSelectorOld = GlobalData.DbSelectorPrevious;
            var dbSelector = GlobalData.DbSelector;
            var dbUpdater = GlobalData.DbUpdater;
            var logStates = new LogStates();
            var types = new clsTypes();

            var relationTypes = new RelationTypes();

            var resultPrevious = dbSelectorOld.get_Data_ClassRel(null, true);
            var clusterConnectors = resultPrevious.Select(classRel => new ClusterConnector
            {
                IdLeft = classRel.ID_Class_Left,
                EntityRoleLeft = EntityRole.Class,
                IdRight = classRel.ID_Class_Right,
                EntityRoleRight = classRel.Ontology == types.AttributeType ? EntityRole.Class : 
                                  classRel.Ontology == types.ClassType ? EntityRole.Class :
                                  classRel.Ontology == types.ObjectType ? EntityRole.Object :
                                  EntityRole.RelationType,
                IdRelation = relationTypes.BelongingAttribute.Id,
                ConnectorType = ConnectorType.ClassAttribute,
                WeightPrimary = classRel.Min_Forw,
                WeightSecondary = classRel.Max_Forw,
                WeightSecondaryBack = classRel.Max_Backw
            }).ToList();

            var entityResult = dbUpdater.SaveClusterConnectors(clusterConnectors);
            entityResult.Result.Id.Should().Be(logStates.Success.Id);


        }

        [TestMethod]
        public void MigrateObjRel()
        {
            var dbSelectorOld = GlobalData.DbSelectorPrevious;
            var dbSelector = GlobalData.DbSelector;
            var dbUpdater = GlobalData.DbUpdater;
            var logStates = new LogStates();
            var types = new clsTypes();

            var resultPrevious = dbSelectorOld.get_Data_ObjectRel(null);

            var objectRelations = resultPrevious.Where(item => item.Ontology == types.ObjectType).Select(item => new ClusterConnector
            {
                IdLeft = item.ID_Object,
                EntityRoleLeft = EntityRole.Object,
                IdParentLeft = item.ID_Parent_Object,
                IdRight = item.ID_Other,
                EntityRoleRight = EntityRole.Object,
                IdParentRight = item.ID_Parent_Other,
                ConnectorType = ConnectorType.Full,
                WeightPrimary = item.OrderID,
                IdRelation = item.ID_RelationType
            }).ToList();

            var connectorResult = dbUpdater.SaveClusterConnectors(objectRelations);

            var classRelations = resultPrevious.Where(item => item.Ontology == types.ClassType).Select(item => new ClusterConnector
            {
                IdLeft = item.ID_Object,
                EntityRoleLeft = EntityRole.Object,
                IdParentLeft = item.ID_Parent_Object,
                IdRight = item.ID_Other,
                EntityRoleRight = EntityRole.Class,
                ConnectorType = ConnectorType.Partial,
                WeightPrimary = item.OrderID,
                IdRelation = item.ID_RelationType
            }).ToList();

            connectorResult = dbUpdater.SaveClusterConnectors(classRelations);

            var attributeTypeRelations = resultPrevious.Where(item => item.Ontology == types.AttributeType).Select(item => new ClusterConnector
            {
                IdLeft = item.ID_Object,
                EntityRoleLeft = EntityRole.Object,
                IdParentLeft = item.ID_Parent_Object,
                IdRight = item.ID_Other,
                EntityRoleRight = EntityRole.Class,
                ConnectorType = ConnectorType.Partial,
                WeightPrimary = item.OrderID,
                IdRelation = item.ID_RelationType
            }).ToList();

            connectorResult = dbUpdater.SaveClusterConnectors(attributeTypeRelations);

            var relationTypeRelations = resultPrevious.Where(item => item.Ontology == types.RelationType).Select(item => new ClusterConnector
            {
                IdLeft = item.ID_Object,
                EntityRoleLeft = EntityRole.Object,
                IdParentLeft = item.ID_Parent_Object,
                IdRight = item.ID_Other,
                EntityRoleRight = EntityRole.RelationType,
                ConnectorType = ConnectorType.Partial,
                WeightPrimary = item.OrderID,
                IdRelation = item.ID_RelationType
            }).ToList();

            connectorResult = dbUpdater.SaveClusterConnectors(attributeTypeRelations);
        }

        [TestMethod]
        public void GetClasses()
        {
            var dbSelector = GlobalData.DbSelector;
            var entities =
                dbSelector.GetDataEntities(new List<ClusterEntity> {new ClusterEntity { EntityRole = EntityRole.Class }});

            entities.ResultList.Count.Should().BeGreaterThan(0);
        }

        [TestMethod]
        public void GetClassTree()
        {
            var dbSelector = GlobalData.DbSelector;

            var classes = dbSelector.GetDataEntities(new List<ClusterEntity> { new ClusterEntity { EntityRole = EntityRole.Class } });
            var rootClass = classes.ResultList.FirstOrDefault(classItem => classItem.ParentIds == null || (classItem.ParentIds.FirstOrDefault() == null));

            var rootClassTreeItem = new ClassTreeItem
            {
                Class = rootClass,
                ClassIdPath = rootClass.Id,
                ClassNamePath = rootClass.Name
            };

            AddSubClasses(rootClassTreeItem, classes.ResultList);

        }

        public void AddSubClasses(ClassTreeItem parentClass, List<ClusterEntity> classes)
        {
            var subClasses = classes.Where(cls => cls.ParentIds.Contains(parentClass.Class.Id)).Select(cls => new ClassTreeItem
            {
                Class = cls,
                ParentTreeItem = parentClass,
                ClassIdPath = parentClass.ClassIdPath + cls.Id,
                ClassNamePath = $"{ parentClass.ClassNamePath } \\ { cls.Name }"
            }).ToList();

            parentClass.SubClasses = subClasses;

            parentClass.SubClasses.ForEach(subCls =>
            {
                AddSubClasses(subCls, classes);
            });

        }

        [TestMethod]
        public void GetRelationTypes()
        {
            var dbSelector = GlobalData.DbSelector;
            var entities =
                dbSelector.GetDataEntities(new List<ClusterEntity> { new ClusterEntity { EntityRole = EntityRole.RelationType } });

            entities.ResultList.Count.Should().BeGreaterThan(0);
        }

        [TestMethod]
        public void GetObjects()
        {
            var dbSelector = GlobalData.DbSelector;
            var entities =
                dbSelector.GetDataEntities(new List<ClusterEntity> { new ClusterEntity { EntityRole = EntityRole.Object } });

            entities.ResultList.Count.Should().BeGreaterThan(0);
        }

        [TestMethod]
        public void GetClassAttributes()
        {
            var dbSelector = GlobalData.DbSelector;
            var clusterConnectors = dbSelector.GetDataConnectors(ConnectorType.ClassAttribute);

        }
    }
}
