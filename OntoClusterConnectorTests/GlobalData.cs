﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchNestConnector;
using Nest;
using OntoClusterConnector;

namespace OntoClusterConnectorTests
{
    public static class GlobalData
    {

        public static string Host => "localhost";
        public static int Port => 9200;

        public static string Index => "ontomod";
        public static string IndexRep => "ontomodrep";
        public static string IndexPrevious => "ontology_db";

        public static string IndexRepPrevious => "reports";

        public static int PackageSize => 5000;

        private static string sessionId;
        public static string SessionId
        {
            get
            {
                if (string.IsNullOrEmpty(sessionId))
                {
                    sessionId = Guid.NewGuid().ToString();
                }
                return sessionId;
            }

        }

        public static DbSelector DbSelector
            => new DbSelector(Host, Port, Index, IndexRep, PackageSize, SessionId);

        public static DbUpdater DbUpdater => new DbUpdater(DbSelector);

        public static clsDBSelector DbSelectorPrevious => new clsDBSelector(Host, Port, IndexPrevious, IndexRepPrevious, PackageSize, SessionId);
    }
}
